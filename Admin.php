<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = 'admin';

    protected $primaryKey = 'admin_id';

    protected $fillable = ['admin_id', 'username', 'password', 'pwd_show', 'nav', 'status', 'ga_secret', 'ga_open'];
}
