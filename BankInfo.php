<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankInfo extends Model
{
    protected $table = 'bank_info';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'bank_code',
        'bank_name',
        'updated_at',
        'created_at',
    ];

}
