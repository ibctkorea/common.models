<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chart extends Model
{
    protected $table = 'chart';

    protected $primaryKey = 'id';
    protected $fillable = ['id','base_currency_id','target_currency_id','time','total','start','min','max','end','flag','created_at','updated_at','type'];

}
