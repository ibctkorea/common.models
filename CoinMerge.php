<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CoinMerge extends Model
{
    protected $table = 'coin_merge';

    protected $fillable = ['currency_info_id', 'address', 'amount', 'coin_transaction_id', 'hash', 'send_amount', 'status'];
}