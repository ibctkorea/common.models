<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoinTransaction extends Model
{
    protected $table = 'coin_transaction';

    protected $primaryKey = 'id';

    protected $fillable = ['member_id', 'type', 'currency_id', 'hash', 'sender', 'receiver', 'amount', 'fee', 'status', 'blocknumber', 'receiver_sub', 'tx_index', 'confirm', 'daemon_tx_id', 'created_at', 'updated_at', 'deleted_at'];

    protected $hidden = ['deleted_at'];
}