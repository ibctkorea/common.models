<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'currency';

    protected $primaryKey = 'currency_id';

    protected $fillable = ['currency_name', 'currency_name_en' ,'currency_name_kr' ,'currency_logo' ,'currency_mark' ,'currency_content' ,'currency_all_money' ,'currency_all_num' ,'currency_buy_fee' ,'currency_sell_fee' ,'currency_url' ,'trade_currency_id' ,'is_line' ,'is_lock' ,'port_number' ,'add_time' ,'status' ,'rpc_url' ,'rpc_pwd' ,'rpc_user' ,'currency_all_tibi' ,'detail_url' ,'qianbao_url' ,'qianbao_key' ,'price_up' ,'price_down' ,'sort' ,'currency_digit_num' ,'guanwang_url' ,'currency_intro' ,'is_withdraw' ,'is_recharge' ,'block_type' ,'is_eth_token' ,'ori_priceinusdt' ,'ori_priceinbtc' ,'ori_priceineth' ,'tibi_fee' ,'auto_cb' ,'confirms' ,'ori_priceinkrw'];

    public function children()
    {
        return $this->hasMany('App\Models\Currency', 'parent_id', 'currency_id');
    }

    public function token()
    {
        return $this->hasMany('App\Models\CurrencyToken', 'currency_id', 'currency_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Currency', 'parent_id', 'currency_id');
    }
}
