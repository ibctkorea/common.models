<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrencyCt extends Model
{
    protected $table = 'currency_ct';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = ['currency_id', 'min_num', 'trans_query_url', 'confirms', 'auto_recharge', 'withdraw_fee', 'is_withdraw', 'is_recharge', 'currency_precision', 'withdraw_limit', 'generate_address_active'];

    public function currency()
    {
        return $this->belongsTo('App\Models\CurrencyInfo', 'currency_id', 'id');
    }
}
