<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrencyInfo extends Model
{
    protected $table = 'currency_info';

    protected $primaryKey = 'id';

    protected $fillable = ['symbol', 'currency_name_en' ,'currency_name_kr' ,'parent_id' ,'decimal_point' ,'fee_buy' ,'fee_sell' ,'trade_status' ,'deposit_status' ,'withdraw_status', 'merge_active', 'active', 'address_type','sort'];

    public function children()
    {
        return $this->hasMany('App\Models\CurrencyInfo', 'parent_id', 'id');
    }

    public function token()
    {
        return $this->hasMany('App\Models\CurrencyToken', 'currency_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\CurrencyInfo', 'parent_id', 'id');
    }
}
