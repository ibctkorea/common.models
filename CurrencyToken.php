<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrencyToken extends Model
{
    protected $table = 'currency_token';

    protected $primaryKey = 'id';

    protected $fillable = ['currency_id' ,'token_id' ,'created_at' ,'updated_at'];

    public function currency()
    {
        return $this->belongsTo('App\Models\CurrencyInfo', 'currency_id', 'id');
    }
}
