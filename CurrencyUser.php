<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrencyUser extends Model
{
    protected $table = 'currency_user';

    protected $primaryKey = 'cu_id';

    protected $fillable = ['member_id' ,'currency_id' ,'num' ,'forzen_num' ,'status' ,'chongzhi_url' ,'block_type' ,'createtime' ,'updatetime' ,'flag' ,'locked_num' ,'unlock_wait_num' ,'locked_num2' ,'locked_num3' ,'unlock_wait_num2'];

    public $timestamps = false;

    public function currency()
    {
        return $this->belongsTo('App\Models\Currency', 'currency_id', 'currency_id');
    }

    public function member()
    {
        return $this->belongsTo('App\Models\Member', 'member_id', 'member_id');
    }
}
