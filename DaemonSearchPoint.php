<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DaemonSearchPoint extends Model
{
    protected $table = 'daemon_search_point';

    protected $fillable = ['type', 'currency_id', 'point', 'created_at', 'updated_at'];

}
