<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DaemonTx extends Model
{
    protected $table = 'daemon_tx';

    protected $fillable = ['currency_id', 'type', 'hash', 'tx_index', 'blockhash', 'blocknumber', 'sender', 'receiver', 'receiver_sub', 'amount', 'fee', 'token_id', 'block_time', 'coin_id', 'confirm', 'status', 'status_deposit'];

    public function currency()
    {
        return $this->belongsTo('App\Models\CurrencyInfo', 'currency_id', 'id');
    }
}
