<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    protected $table = 'deposit';
    protected $primaryKey = 'deposit_id';
    protected $fillable = [
        'deposit_id',
        'member_id',
        'member_name',
        'nAmount1',
        'nAmount2',
        'sBank',
        'sAccount',
        'sOwner',
        'sCode',
        'nStatus',
        'regDate',
        'editDate',
        'ip'
    ];
}

