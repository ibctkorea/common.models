<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExaminePwdtrade extends Model
{
    protected $table = 'examine_pwdtrade';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'u_type',
        'u_id',
        'uname',
        'idcard',
        'idcardPositive',
        'idcardSide',
        'idcardHold',
        'add_time',
        'examine_otime',
        'examine_ttime',
        'examine_oid',
        'examine_tid',
        'status',
        'surname',
        'sex',
        'state',
        'passportID',
        'up_time',
        'cause'
    ];

}
