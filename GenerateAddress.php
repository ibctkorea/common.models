<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenerateAddress extends Model
{
    protected $table = 'generate_address';

    protected $fillable = ['currency_id','address','status','member_id','created_at','updated_at'];
}