<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenerateAddressTransaction extends Model
{
    protected $table = 'generate_address_transaction';

    protected $fillable = ['currency_info_id','hash','status'];
}