<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemBank extends Model
{
    protected $table = 'mem_bank';
    protected $primaryKey = 'seq';
    protected $fillable = [
        'seq',
        'member_id',
        'bank_code',
        'bank_accnt',
        'bank_owner',
        'stNo',
        'regdate',
        'editdate',
        'isdel',
        'ip'
    ];
}

