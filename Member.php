<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table = 'member';

    protected $primaryKey = 'member_id';

    protected $fillable = ['openid' ,'email' ,'pwd' ,'pid' ,'pid_type' ,'pwdtrade' ,'nick' ,'name' ,'cardtype' ,'idcard' ,'area_code' ,'phone' ,'ip' ,'reg_time' ,'login_ip' ,'login_time' ,'vip_level' ,'vip_end_time' ,'rmb' ,'forzen_rmb' ,'head' ,'profile' ,'province' ,'city' ,'job' ,'is_lock' ,'status' ,'dividend_num' ,'threepwd' ,'ga_secret' ,'invitation' ,'user_prove' ,'ga_setting' ,'ga_open' ,'ga_video' ,'one_charge' ,'is_robot' ,'show_uid' ,'reg_type' ,'member_type' ,'leader_invitation' ,'milk_lock_rate' ,'member_source' ,'register_endpoint' ,'milk_lock_rate2' ,'sex' ,'user_type'];
}
