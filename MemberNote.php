<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberNote extends Model
{
    protected $table = 'member_note';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'member_id',
        'type',
        'message',
        'status',
        'updated_at',
        'created_at'
    ];

}
