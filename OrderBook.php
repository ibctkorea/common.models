<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderBook extends Model
{
    use SoftDeletes;
    protected $table = 'order_book';

    protected $fillable = ['member_id','type','base_currency_id','price','target_currency_id','target_amount','remain_amount','deleted_at'];
}
