<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Statistics extends Model
{
    protected $table = 'statistics';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'currency_id',
        'symbol',
        'deposit_cnt',
        'deposit_amount',
        'withdraw_cnt',
        'withdraw_amount',
        'trade_amount',
        'trade_amount_krw',
        'trade_fee',
        'withdraw_fee',
        'merge_fee',
        'gas_fee',
        'created_at',
        'updated_at',
    ];
}

