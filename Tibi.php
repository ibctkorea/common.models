<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tibi extends Model
{
    protected $table = 'tibi';

    protected $primaryKey = 'id';

    protected $fillable = ['user_id','url','name','add_time','num','status','ti_id','check_time','currency_id','fee','actual','auditor','bcstatus','bc_no','bc_from','bc_confirms','bc_ts','bc_value','bc_hash','transfer_result','transfer_status','fee_currency_id','site_label','autotb_result','total_cb','total_tb','total_assets','estimated_num','onekeytb_result'];
}
