<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TradeContract extends Model
{
    protected $table = 'trade_contract';

    protected $fillable = ['price', 'amount', 'base_currency_id', 'target_currency_id', 'buy_order_id', 'buy_member_id', 'buy_fee', 'sell_order_id', 'sell_member_id', 'sell_fee'];
}
