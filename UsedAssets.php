<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsedAssets extends Model
{
    use SoftDeletes;

    protected $table = 'used_assets';

    protected $primaryKey = 'id';

    protected $fillable = ['type', 'member_id', 'currency_id', 'amount', 'ref_id', 'created_at', 'updated_at'];
}