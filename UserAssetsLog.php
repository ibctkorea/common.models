<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAssetsLog extends Model
{
    protected $table = 'user_assets_log';

    protected $primaryKey = 'id';

    protected $fillable = ['type', 'member_id', 'currency_id', 'before_amount', 'amount', 'ref_id', 'created_at', 'updated_at'];
}