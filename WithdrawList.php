<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WithdrawList extends Model
{
    protected $table = 'withdrawlist';
    protected $primaryKey = 'seq';
    protected $fillable = [
        'seq',
        'member_id',
        'amount',
        'fees',
        'amountdue',
        'bank_code',
        'bank_name',
        'bank_accnt',
        'regdate',
        'editdate',
        'nstatus',
        'memo',
        'ip'
    ];
}

